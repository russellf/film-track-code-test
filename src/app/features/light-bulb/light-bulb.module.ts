import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LightBulbRoutingModule, routedComponents } from './light-bulb-routing/light-bulb-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    LightBulbRoutingModule,
  ],
  declarations: [
    ...routedComponents
  ]
})
export class LightBulbModule { }

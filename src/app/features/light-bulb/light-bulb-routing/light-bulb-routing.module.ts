import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LightBulbFormComponent } from '../light-bulb-form/light-bulb-form.component';
import { RouterModule } from '@angular/router';

const routes = [
  {
    path: '',
    component: LightBulbFormComponent
  },
  {
    path: ':lightbulbs/people/:people',
    component: LightBulbFormComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})
export class LightBulbRoutingModule { }

export const routedComponents = [
  LightBulbFormComponent
];

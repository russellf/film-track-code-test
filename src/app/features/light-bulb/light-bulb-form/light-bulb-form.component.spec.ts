import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LightBulbFormComponent } from './light-bulb-form.component';
import { Inject, inject, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

class ActivatedRouteMock {

}

class RouterMock {

}


describe('LightBulbFormComponent', () => {
  let component: LightBulbFormComponent;
  let fixture: ComponentFixture<LightBulbFormComponent>;

  let mocks: {
    route: ActivatedRoute,
    init: () => void
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LightBulbFormComponent ],
      providers: [
        FormBuilder,
        {provide: ActivatedRoute, useClass: ActivatedRouteMock},
        {provide: Router, useClass: RouterMock}
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LightBulbFormComponent);
    component = fixture.componentInstance;
  });


  beforeEach(() => {
    const route = fixture.debugElement.injector.get(ActivatedRoute);
    mocks = {
      route,
      init: component.ngOnInit
    };
    spyOn(component, 'ngOnInit')
      .and
      .callFake(() => {});

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

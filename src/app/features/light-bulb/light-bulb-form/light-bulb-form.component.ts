import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/index';
import { map } from 'rxjs/internal/operators';
import { ActivatedRoute, NavigationExtras, ParamMap, Route, Router } from '@angular/router';

@Component({
  selector: 'app-light-bulb-form',
  templateUrl: './light-bulb-form.component.html',
  styleUrls: ['./light-bulb-form.component.scss']
})
export class LightBulbFormComponent implements OnInit {

  formGroup$: Observable<FormGroup>;

  /**
   * Class Constructor Function
   * @param {FormBuilder} _formBuilder
   */
  constructor(private _formBuilder: FormBuilder, private _route: ActivatedRoute, public _router: Router) { }

  /**
   * Angular Lifecycle hook oninit
   */
  ngOnInit() {
    this.buildForm();
  }

  /**
   * Build form function, will use form builder to create the FormGroup for the page
   */
  buildForm() {
    this.formGroup$ = this._route.paramMap.pipe(
      map((params: ParamMap) => {
        return this._formBuilder
          .group(
            {
              lightbulbs: [params.get('lightbulbs'), [Validators.required, Validators.pattern(/^\d+$/)]],
              people: [params.get('people'), [Validators.required, Validators.pattern(/^\d+$/)]]
            }
          );
      })
    );
  }

  submit(event: Event, formGroup: FormGroup) {
    console.log(formGroup);
    if (formGroup.valid) {
      const values = formGroup.value;
      this._router.navigate([
        '/',
        'lightbulbs',
        values.lightbulbs,
        'people',
        values.people]);
    }
  }

}
